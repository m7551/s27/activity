let http = require('http');

// Mock Items Array
let items = [
	{
		name: "Iphone X",
		description: "Phone designed and created by Apple",
		price: 30000
	},
	{
		name: "Horizon Forbidden West",
		description: "Newest game for the PS4 and PS5",
		price: 4000
	},
	{
		name: "Razer Tiamat",
		description: "Headset from Razer",
		price: 3000
	}
];



http.createServer((req,res) => {

	if (req.url === '/items' && req.method === "GET"){
		res.writeHead(200, {'Content-type': 'application/json'});
		res.end(JSON.stringify(items));
	}

	if (req.url === '/items' && req.method === "POST"){
		let newItem = "";

		req.on('data', (data) => {
			newItem += data;
		});

		req.on('end', () => {
			newItem = JSON.parse(newItem);

			items.push(newItem);

			res.writeHead(200, {'Content-type': 'application/json'});
			res.end(JSON.stringify(items));
		})
	}

	//Stretch Goals:
	
	if (req.url === '/items/findItem' && req.method === "POST"){
		let search = "";
		req.on('data', (data) => {
			 search += data;
		});

		req.on('end', () => {
			search = JSON.parse(search);

			const result=items.find(i => i.name == search.name);

			res.writeHead(200, {'Content-type': 'application/json'});
			res.end(JSON.stringify(result))
		})
	}

}).listen(8000);

console.log("Server is currently running on port: 8000")